FROM centos:7
RUN yum install -y epel-release && yum install -y --enablerepo=epel \
  doxygen \
  elfutils-devel \
  file \
  gcc \
  gcc-c++ \
  git \
  glibc-devel \
  glibc-devel.i686 \
  gtest-devel \
  kernel-devel \
  libarchive-devel \
  make \
  openssl-devel \
  procps-devel \
  python-devel \
  python-setuptools \
  python-wheel \
  python34-devel \
  python34-setuptools \
  python34-wheel \
  rpm-build \
  rsync \
  ruby \
  subversion \
  unzip \
  wget \
  which \
  xz-devel \
  zip \
  && rm -rf /var/cache/yum/* \
  && yum clean all

# Python
RUN easy_install pip && pip install numpy
RUN easy_install-3.4 pip && pip3 install numpy

# cmake
RUN wget -q https://cmake.org/files/v3.11/cmake-3.11.1-Linux-x86_64.tar.gz \
  && tar xf cmake-3.11.1-Linux-x86_64.tar.gz \
  && cp -r ./cmake-3.11.1-Linux-x86_64/* /usr/ \
  && rm -rf ./cmake-3.11.1-Linux-x86_64*

# Rust
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain stable
ENV PATH $PATH:/root/.cargo/bin
RUN rustup component add rust-src rustfmt-preview
RUN cargo install cargo-check llvmenv -f

# pixz
RUN wget -q https://github.com/vasi/pixz/releases/download/v1.0.6/pixz-1.0.6.tar.xz \
  && tar xf pixz-1.0.6.tar.xz \
  && pushd pixz-1.0.6 \
  && ./configure --prefix=/usr \
  && make -j \
  && make install \
  && popd \
  && rm -rf pixz-1.0.6*

WORKDIR /src
